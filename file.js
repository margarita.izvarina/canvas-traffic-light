export function createTrafficLight(){

	var canvas = document.querySelector('#canv');
	var pen = canvas.getContext('2d');

	pen.fillStyle = "#292e38";

	pen.beginPath();
	pen.moveTo( 50, 60 );
	pen.quadraticCurveTo( 55, 55, 60, 50 );
	pen.lineTo( 220, 50 );
	pen.quadraticCurveTo( 225, 55, 230, 60 );
	pen.lineTo( 230, 340 );
	pen.quadraticCurveTo( 225, 345, 220, 350 );
	pen.lineTo( 60, 350 );
	pen.quadraticCurveTo( 55, 345, 50, 340 );
	pen.lineTo( 50, 60 );
	pen.fill();
	pen.closePath();

	createLights();

}

function createLights(){

	var canvas = document.querySelector('#canv');
	var pen = canvas.getContext('2d');

	var tmp = 110;
	for(var i = 0; i < 3; i++){
		pen.beginPath();
		pen.arc(140, tmp, 35, 0, 2*Math.PI, false);
		pen.fillStyle = "#e0cb1d";
		pen.fill();
		pen.closePath();
		tmp += 90;
	}

}

createTrafficLight();
